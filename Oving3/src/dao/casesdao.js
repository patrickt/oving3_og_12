// @flow
/* eslint eqeqeq: "off" */

const Dao = require("./dao.js");

module.exports = class CasesDao extends Dao {
  getAll(callback) {
    super.query(
      "select id, title, titleText, content, date, picture, category, importance from newsCase",
      [],
      callback
    );
  }

  getOne(id, callback) {
    super.query(
      "select title, titleText, content, date, picture, category, importance from newsCase where id=?",
      [id],
      callback
    );
  }

  getNews(callback) {
    super.query(
      "select title, titleText, content, date, picture, category, importance from newsCase where category='Nyheter'",
      [],
      callback
    );
  }

  getSports(callback) {
    super.query(
      "select title, titleText, content, date, picture, category, importance from newsCase where category='Sport'",
      [],
      callback
    );
  }

  getPolitics(callback) {
    super.query(
      "select title, titleText, content, date, picture, category, importance from newsCase where category='Politikk'",
      [],
      callback
    );
  }

  getCulture(callback) {
    super.query(
      "select title, titleText, content, date, picture, category, importance from newsCase where category='Kultur'",
      [],
      callback
    );
  }

  getGames(callback) {
    super.query(
      "select title, titleText, content, date, picture, category, importance from newsCase where category='Dataspill'",
      [],
      callback
    );
  }

  createOne(json, callback) {
    var val = [
      json.title,
      json.titleText,
      json.content,
      json.date,
      json.picture,
      json.category,
      json.importance
    ];
    super.query(
      "insert into newsCase (title,titleText,content,date,picture,category,importance) values (?,?,?,?,?,?,?)",
      val,
      callback
    );
  }

  updateOne(json, id, callback) {
    var val = [
      json.title,
      json.titleText,
      json.content,
      json.date,
      json.picture,
      json.category,
      json.importance
    ];
    super.query(
      "update newsCase set (title,titleText,content,date,picture,category,importance) values(?,?,?,?,?,?,?) where id=?",
      [id],
      callback
    );
  }

  deleteOne(id, callback) {
    super.query("delete from newsCase where id=?", [id], callback);
  }
};
